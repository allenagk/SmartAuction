package com.android.smartauction;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Example local unit test, which will execute on the development machine (host).
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
public class ExampleUnitTest {
    @Test
    public void addition_isCorrect() {
        assertEquals(4, 2 + 2);
    }

    @Test
    public void slit_from_underscore() {
        String value = "1ihzVhg68uVxNeW5hysStEG01dN2_6sbY2a6cP1eCEJLHAaVjfMPwoZv1";
        String actual[] = value.split("_");
        assertEquals("1ihzVhg68uVxNeW5hysStEG01dN2", actual[0]);
    }
}