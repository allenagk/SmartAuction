package com.android.smartauction.service;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.util.Log;
import android.widget.Toast;

import com.android.smartauction.R;
import com.android.smartauction.activity.ListActivity;
import com.android.smartauction.model.Post;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.sql.Date;
import java.util.List;

import static com.android.smartauction.AppConstants.MY_AUCTIONS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.MY_BIDS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.USER_ID_BUNDLE_KEY;

/**
 * An {@link IntentService} subclass for handling asynchronous task requests in
 * a service on a separate handler thread.
 * <p>
 * TODO: Customize class - update intent actions, extra parameters and static
 * helper methods.
 */
public class APIService extends IntentService {
    // TODO: Rename actions, choose action names that describe tasks that this
    // IntentService can perform, e.g. ACTION_FETCH_NEW_ITEMS
    private static final String ACTION_FOO = "com.android.smartauction.service.action.FOO";
    private static final String ACTION_BAZ = "com.android.smartauction.service.action.BAZ";

    // TODO: Rename parameters
    private static final String EXTRA_PARAM1 = "com.android.smartauction.service.extra.PARAM1";
    private static final String EXTRA_PARAM2 = "com.android.smartauction.service.extra.PARAM2";

    private static String TAG = "APIService";

    public APIService() {
        super("APIService");
    }

    private FirebaseFirestore db;
    private String userid;


    /**
     * Starts this service to perform action Foo with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionFoo(Context context, String param1, String param2) {
        Intent intent = new Intent(context, APIService.class);
        intent.setAction(ACTION_FOO);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    /**
     * Starts this service to perform action Baz with the given parameters. If
     * the service is already performing a task this action will be queued.
     *
     * @see IntentService
     */
    // TODO: Customize helper method
    public static void startActionBaz(Context context, String param1, String param2) {
        Intent intent = new Intent(context, APIService.class);
        intent.setAction(ACTION_BAZ);
        intent.putExtra(EXTRA_PARAM1, param1);
        intent.putExtra(EXTRA_PARAM2, param2);
        context.startService(intent);
    }

    private void init() {
        db = FirebaseFirestore.getInstance();

    }

    //Set extras received by mainActivity
    private void setExtras(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            if (intent.hasExtra(USER_ID_BUNDLE_KEY)) {
                userid = extras.getString(USER_ID_BUNDLE_KEY);
            }
            /*if (intent.hasExtra(MY_BIDS_BUNDLE_KEY)) {
                isMybids = intent.getBooleanExtra(MY_BIDS_BUNDLE_KEY, false);
            }

            if (intent.hasExtra(MY_AUCTIONS_BUNDLE_KEY)) {
                isMyAuctions = intent.getBooleanExtra(MY_AUCTIONS_BUNDLE_KEY, false);
            }*/
        } else {
            Log.d(TAG, "No Extras");
        }
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Log.d(TAG, "Service Started");
        init();
        setExtras(intent);
        handlerEvent(this);     //auction side
        handlerEvent2(this);       //customer side

       /* if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_FOO.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionFoo(param1, param2);
            } else if (ACTION_BAZ.equals(action)) {
                final String param1 = intent.getStringExtra(EXTRA_PARAM1);
                final String param2 = intent.getStringExtra(EXTRA_PARAM2);
                handleActionBaz(param1, param2);
            }
        }*/
    }

    //Hander event to check Auction end or not (publisher side)
    private void handlerEvent(final Context context) {
        final Handler handler = new Handler(getMainLooper());
        final int delay = 5000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                //do something
                Log.d(TAG, "CurrentTimeInMs : " + System.currentTimeMillis());
                Date curDate = new Date(System.currentTimeMillis());//current time
                checkEndBids(curDate, context);
                handler.postDelayed(this, delay);
            }
        }, delay);

    }

    //Hander event to check Bid has end and won
    private void handlerEvent2(final Context context) {
        final Handler handler = new Handler(getMainLooper());
        final int delay = 5000; //milliseconds

        handler.postDelayed(new Runnable() {
            public void run() {
                //do something
                Date curDate = new Date(System.currentTimeMillis());//current time
                checkWonBids(curDate, context);
                handler.postDelayed(this, delay);
            }
        }, delay);

    }

    private void showToast(String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
    }

    /**
     * Create and push the notification
     */
    public void createNotification(String title, String message, Context mContext, String uid) {
        Log.d(TAG, "TOP NOTIFICATION (END)");
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(this, ListActivity.class);
        resultIntent.putExtra(USER_ID_BUNDLE_KEY, uid);
        resultIntent.putExtra(MY_AUCTIONS_BUNDLE_KEY, true);
        //startActivity(resultIntent);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder mBuilder = new Notification.Builder(mContext);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("channel1", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId("channel1");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }

    /**
     * Check won bids
     */
    private void checkEndBids(final Date todayDate, final Context context) {

        db.collection(POSTS_COLLECTION_REF)
                .whereEqualTo("live", true)
                .whereEqualTo("user_id", userid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            //List<String> list = new ArrayList<>();
                    /*for (QueryDocumentSnapshot document : task.getResult()) {
                        list.add(document.getId());
                    }*/
                            List<Post> posts = task.getResult().toObjects(Post.class);
                            Log.d(TAG, "Posts size: " + posts.size());

                            for (int i = 0; i < posts.size(); i++) {

                                Post post = posts.get(i).withId(task.getResult().getDocuments().get(i).getId());

                                Log.d(TAG, "Document ID : " + post.getId());

                                java.util.Date historyDate = post.getDate();
                                java.util.Date futureDate = post.getEnd_date();

                                if (todayDate.after(historyDate) && todayDate.before(futureDate)) {
                                    // In between
                                    Log.d(TAG, "Date in between");

                                } else {
                                    // if not in between
                                    Log.d(TAG, "Date in not between");

                                    post.setLive(false);
                                    updatePost(post, context, post.getId(), false);
                                }

                            }

                            Log.d(TAG, posts.toString());
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
/*
        Query query = db.collection(POSTS_COLLECTION_REF)
                .whereEqualTo("live", true)
                .whereEqualTo("user_id", userid);

        query.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(QuerySnapshot documentSnapshots, FirebaseFirestoreException e) {
                if (!documentSnapshots.isEmpty()) {


                    //Check product is within start date and endbidDate or beyond
                    List<Post> posts = documentSnapshots.toObjects(Post.class);
                    Log.d(TAG, "Posts size: " + posts.size());

                    for (int i = 0; i < posts.size(); i++) {

                        Post post = posts.get(i).withId(documentSnapshots.getDocuments().get(i).getId());

                        Log.d(TAG, "Document ID : "+post.getId());

                        java.util.Date historyDate = post.getDate();
                        java.util.Date futureDate = post.getEnd_date();

                        if (todayDate.after(historyDate) && todayDate.before(futureDate)) {
                            // In between
                            Log.d(TAG, "Date in between");

                        } else {
                            // if not in between
                            Log.d(TAG, "Date in not between");

                            post.setLive(false);
                            updatePost(post, context, post.getId());
                        }

                    }
                } else {
                    Log.d(TAG, "DocumentSnapshots is empty");
                }
            }
        });*/
    }

    private void updatePost(final Post post, final Context context, String documentID, final boolean isCustomer) {
        db.collection(POSTS_COLLECTION_REF).document(documentID).set(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Log.d(TAG, "UpdatePost Success");

                if (isCustomer) {
                    createWinNotification("Auction Wins", "You have won an auction : " + post.getTitle(), context, userid);
                } else {
                    createNotification("Auction Ended", "Auction title : " + post.getTitle(), context, userid);
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Log.d(TAG, "UpdatePost failure: " + e.getMessage());

            }
        });
    }

    private void checkWonBids(final Date todayDate, final Context context) {

        db.collection(POSTS_COLLECTION_REF)
                .whereEqualTo("live", false)
                .whereEqualTo("max_bid_uid", userid)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {

                            List<Post> posts = task.getResult().toObjects(Post.class);

                            for (int i = 0; i < posts.size(); i++) {

                                Post post = posts.get(i).withId(task.getResult().getDocuments().get(i).getId());


                                if (!post.isNotified()) {
                                    // if not in between
                                    Log.d(TAG, "1st time notification");
                                    post.setLive(false);
                                    post.setNotified(true);
                                    updatePost(post, context, post.getId(), true);
                                } else {
                                    Log.d(TAG, "Already Notified the customer");
                                }


                            }

                            Log.d(TAG, posts.toString());
                        } else {
                            Log.d(TAG, "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    /**
     * Create and push the notification for win user
     */
    public void createWinNotification(String title, String message, Context mContext, String uid) {
        Log.d(TAG, "TOP NOTIFICATION (WIN)");
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(this, ListActivity.class);
        resultIntent.putExtra(USER_ID_BUNDLE_KEY, uid);
        resultIntent.putExtra(MY_BIDS_BUNDLE_KEY, true);
        //startActivity(resultIntent);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        Notification.Builder mBuilder = new Notification.Builder(mContext);
        mBuilder.setSmallIcon(R.mipmap.ic_launcher);
        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        NotificationManager mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel("channel1", "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId("channel1");
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
    }

}
