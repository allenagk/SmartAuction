package com.android.smartauction.model;

import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;

import java.util.Date;

/**
 * Created by savantis-allen on 12/5/18.
 */
public class ChatMessage {

    private String messageText;
    private String messageUser;
    private long messageTime;

    @Exclude
    public String id;

    public ChatMessage(String messageText, String messageUser) {
        this.messageText = messageText;
        this.messageUser = messageUser;

        // Initialize to current time
        messageTime = new Date().getTime();
    }

    public ChatMessage(){

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public <T extends ChatMessage> T withId(@NonNull final String id) {
        this.id = id;
        return (T) this;
    }

    public String getMessageText() {
        return messageText;
    }

    public void setMessageText(String messageText) {
        this.messageText = messageText;
    }

    public String getMessageUser() {
        return messageUser;
    }

    public void setMessageUser(String messageUser) {
        this.messageUser = messageUser;
    }

    public long getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(long messageTime) {
        this.messageTime = messageTime;
    }
}
