package com.android.smartauction.model;


import android.support.annotation.NonNull;

import com.google.firebase.firestore.Exclude;
import com.google.firebase.firestore.GeoPoint;

import java.sql.Timestamp;
import java.util.Date;


public class Post {

    //private String uid;
    private String title;
    private String desc;
    private String price;
    private Date date;
    private String photo_url1;
    private String user_id;
    private String max_bid_uid;
    private boolean live;
    private String current_bid;
    private Date end_date;
    private int bids;
    private GeoPoint geo_point;
    private String product_address;
    private boolean notified;

    @Exclude
    public String id;


    public Post() {
    }

    //All required fields only
    public Post(String user_id, String title, String desc, String price, GeoPoint geo_point) {
        //this.uid = uid;
        this.user_id = user_id;
        this.title = title;
        this.desc = desc;
        this.price = price;
        this.geo_point = geo_point;
        //this.photo_url1 = url;
        this.date = new Timestamp(System.currentTimeMillis());
    }

    public <T extends Post> T withId(@NonNull final String id) {
        this.id = id;
        return (T) this;
    }

    public String getId() {
        return id;
    }

    public boolean isNotified() {
        return notified;
    }

    public void setNotified(boolean notified) {
        this.notified = notified;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProduct_address() {
        return product_address;
    }

    public void setProduct_address(String product_address) {
        this.product_address = product_address;
    }

    public GeoPoint getGeo_point() {
        return geo_point;
    }

    public void setGeo_point(GeoPoint geo_point) {
        this.geo_point = geo_point;
    }

    public String getMax_bid_uid() {
        return max_bid_uid;
    }

    public void setMax_bid_uid(String max_bid_uid) {
        this.max_bid_uid = max_bid_uid;
    }

    public int getBids() {
        return bids;
    }

    public void setBids(int bids) {
        this.bids = bids;
    }

    public Date getEnd_date() {
        return end_date;
    }

    public void setEnd_date(Date end_date) {
        this.end_date = end_date;
    }

    public String getCurrent_bid() {
        return current_bid;
    }

    public void setCurrent_bid(String current_bid) {
        this.current_bid = current_bid;
    }

    public boolean isLive() {
        return live;
    }

    public void setLive(boolean live) {
        this.live = live;
    }


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }


    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getPhoto_url1() {
        return photo_url1;
    }

    public void setPhoto_url1(String photo_url1) {
        this.photo_url1 = photo_url1;
    }

}
