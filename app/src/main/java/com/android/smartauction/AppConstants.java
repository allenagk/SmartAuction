package com.android.smartauction;

public class AppConstants {

    //Firebase Database References
    public static final String USERS_COLLECTION_REF = "users";
    public static final String POSTS_COLLECTION_REF = "posts";
    public static final String MESSAGES_COLLECTION_REF = "messages";


    //Firebase Storage References
    public static final String POST_IMAGES_STORAGE_REF = "post_images";

    //Bundle keys
    public static final String POST_DOC_ID_BUNDLE_KEY = "post_key";
    public static final String USER_ID_BUNDLE_KEY = "user_id_key";
    public static final String MY_BIDS_BUNDLE_KEY = "my_bids_key";
    public static final String MY_AUCTIONS_BUNDLE_KEY = "my_auctions_key";
    public static final String FROM_AUCTIONS_BUNDLE_KEY = "from_my_auctions_key";
    public static final String FROM_MY_BIDS_BUNDLE_KEY = "from_my_bids_key";


    //Parcelable data between productActivity and mapActivity
    public static final String PARCELABLE_LAT_REF = "latitude";
    public static final String PARCELABLE_LONG_REF = "longitude";
    public static final String PARCELABLE_ADDRESS_REF = "address";
    public static final String ADMIN_BUNDLE_KEY = "admin";

}
