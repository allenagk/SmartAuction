package com.android.smartauction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.smartauction.R;
import com.android.smartauction.adapter.PostViewHolder;
import com.android.smartauction.model.Post;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.android.smartauction.AppConstants.FROM_AUCTIONS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.FROM_MY_BIDS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.MY_AUCTIONS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.MY_BIDS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.USER_ID_BUNDLE_KEY;
import static com.android.smartauction.Util.getLocalPrice;

public class ListActivity extends AppCompatActivity {

    private final static String TAG = "ListActivity";

    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mPostList;
    private FirebaseFirestore db;
    private boolean isMybids = false;
    private boolean isMyAuctions = false;
    private String userid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        db = FirebaseFirestore.getInstance();

        mPostList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mPostList.addItemDecoration(new DividerItemDecoration(mPostList.getContext(),
                LinearLayoutManager.VERTICAL));
        mPostList.setLayoutManager(linearLayoutManager);
        mPostList.setItemAnimator(new DefaultItemAnimator());


        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(USER_ID_BUNDLE_KEY)) {
                userid = extras.getString(USER_ID_BUNDLE_KEY);
            }
            if (getIntent().hasExtra(MY_BIDS_BUNDLE_KEY)) {
                isMybids = getIntent().getBooleanExtra(MY_BIDS_BUNDLE_KEY, false);
            }

            if (getIntent().hasExtra(MY_AUCTIONS_BUNDLE_KEY)) {
                isMyAuctions = getIntent().getBooleanExtra(MY_AUCTIONS_BUNDLE_KEY, false);
            }
        } else {
            Log.d(TAG, "No Extras");
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (isMyAuctions) {
            getMyAuctionsList();
        }

        if (isMybids) {
            getMyBidsList();
        }
    }

    //get my bids list (won list)
    private void getMyBidsList() {

        Query query = db.collection(POSTS_COLLECTION_REF)
                .whereEqualTo("live", false)
                .whereEqualTo("max_bid_uid", userid);

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setStart_bid(getLocalPrice(model.getPrice()));
                holder.setCurrent_bid(getLocalPrice(model.getCurrent_bid()));
                holder.setStart_date(model.getDate().toString());
                holder.setEnd_date(model.getEnd_date().toString());
                holder.setBids(model.getBids());

                holder.setImageView(model.getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                //Log.d("RESULT DATE: ", model.getDate().toString());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ListActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        intent.putExtra(FROM_MY_BIDS_BUNDLE_KEY, true);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    //get auctions which has userid equals to loggedin user
    private void getMyAuctionsList() {

        Query query = db.collection(POSTS_COLLECTION_REF)
                .whereEqualTo("live", false)
                .whereEqualTo("user_id", userid);

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setStart_bid(getLocalPrice(model.getPrice()));
                holder.setCurrent_bid(getLocalPrice(model.getCurrent_bid()));
                holder.setStart_date(model.getDate().toString());
                holder.setEnd_date(model.getEnd_date().toString());
                holder.setBids(model.getBids());

                holder.setImageView(model.getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                //Log.d("RESULT DATE: ", model.getDate().toString());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(ListActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        intent.putExtra(FROM_AUCTIONS_BUNDLE_KEY, true);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    //For star feature
    /*private void onStarClicked(DatabaseReference postRef) {
        postRef.runTransaction(new Transaction.Handler() {
            @Override
            public Transaction.Result doTransaction(MutableData mutableData) {
                Post p = mutableData.getValue(Post.class);
                if (p == null) {
                    return Transaction.success(mutableData);
                }

                if (p.stars.containsKey(getUid())) {
                    // Unstar the post and remove self from stars
                    p.starCount = p.starCount - 1;
                    p.stars.remove(getUid());
                } else {
                    // Star the post and add self to stars
                    p.starCount = p.starCount + 1;
                    p.stars.put(getUid(), true);
                }

                // Set value and report transaction success
                mutableData.setValue(p);
                return Transaction.success(mutableData);
            }

            @Override
            public void onComplete(DatabaseError databaseError, boolean b,
                                   DataSnapshot dataSnapshot) {
                // Transaction completed
                Log.d(TAG, "postTransaction:onComplete:" + databaseError);
            }
        });
    }*/
}
