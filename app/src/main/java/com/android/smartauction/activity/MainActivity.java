package com.android.smartauction.activity;

import android.app.ActivityManager;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.smartauction.AppConstants;
import com.android.smartauction.R;
import com.android.smartauction.adapter.PostViewHolder;
import com.android.smartauction.model.Post;
import com.android.smartauction.model.User;
import com.android.smartauction.service.APIService;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.List;

import static com.android.smartauction.AppConstants.MY_AUCTIONS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.MY_BIDS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.USERS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.USER_ID_BUNDLE_KEY;
import static com.android.smartauction.Util.getAppVersion;
import static com.android.smartauction.Util.getLocalPrice;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawerLayout;

    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private final static String TAG = "MainActivity";

    TextView tvFullName, tvEmail;
    User muser;
    NavigationView mnavigationView;
    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mPostList;
    private FloatingActionButton fab;
    private Toolbar toolbar;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, AddProductActivity.class);
                startActivity(intent);
            }
        });

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();


        mnavigationView = findViewById(R.id.nav_view);
        View header = mnavigationView.getHeaderView(0);
        tvFullName = header.findViewById(R.id.nav_header_name);
        tvEmail = header.findViewById(R.id.nav_header_email);

        mnavigationView.setNavigationItemSelectedListener(this);

        //Fetch user data and setup navigation drawer admin options
        fetchUserData();


        mPostList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mPostList.addItemDecoration(new DividerItemDecoration(mPostList.getContext(),
                LinearLayoutManager.VERTICAL));
        mPostList.setLayoutManager(linearLayoutManager);
        mPostList.setItemAnimator(new DefaultItemAnimator());

        //Set Fab icon invisible when scrolling
        mPostList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0 && fab.getVisibility() == View.VISIBLE) {
                    fab.hide();
                } else if (dy < 0 && fab.getVisibility() != View.VISIBLE) {
                    fab.show();
                }
            }
        });

    }

    private void getPostList() {

        Query query = db.collection(POSTS_COLLECTION_REF)
                .whereEqualTo("live", true)
                .whereGreaterThanOrEqualTo("end_date", new Timestamp(Calendar.getInstance().getTimeInMillis()))
                .orderBy("end_date");

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setStart_bid(getLocalPrice(model.getPrice()));
                holder.setCurrent_bid(getLocalPrice(model.getCurrent_bid()));
                holder.setStart_date(model.getDate().toString());
                holder.setEnd_date(model.getEnd_date().toString());
                holder.setBids(model.getBids());

                holder.setImageView(model.getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                //Log.d("RESULT DATE: ", model.getDate().toString());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(MainActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    @Override
    protected void onResume() {
        super.onResume();

        getPostList();

        startService();

    }

    private void fetchUserData() {
        mProgressDialog.show();
        Log.d(TAG, mAuth.getUid());
        //tvEmail.setText(mAuth.getCurrentUser().getEmail());

        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(mAuth.getUid());

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                mProgressDialog.hide();
                if (documentSnapshot.exists()) {
                    muser = documentSnapshot.toObject(User.class);
                    if (muser != null) {
                        Log.d(TAG, muser.getFirstName());
                        tvFullName.setText(muser.getFirstName() + " " + muser.getLastName());
                        tvEmail.setText(muser.getEmail());

                        if (muser.isEmployee()) {
                            Menu menuNav = mnavigationView.getMenu();
                            menuNav.findItem(R.id.nav_manage_users).setEnabled(true);
                            menuNav.findItem(R.id.nav_manage_products).setEnabled(true);
                        }

                    }
                } else {
                    Toast.makeText(MainActivity.this,
                            R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                    signOut();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgressDialog.hide();
                Toast.makeText(MainActivity.this,
                        R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                signOut();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main2, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {
            Intent intent = new Intent(this, SearchActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_home) {
            Intent intent = new Intent(MainActivity.this, MainActivity.class);
            startActivity(intent);
            finish();
        } else if (id == R.id.nav_orders) {
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra(USER_ID_BUNDLE_KEY, mAuth.getUid());
            intent.putExtra(MY_BIDS_BUNDLE_KEY, true);
            startActivity(intent);
        } else if (id == R.id.nav_auctions) {
            Intent intent = new Intent(MainActivity.this, ListActivity.class);
            intent.putExtra(USER_ID_BUNDLE_KEY, mAuth.getUid());
            intent.putExtra(MY_AUCTIONS_BUNDLE_KEY, true);
            startActivity(intent);
        } else if (id == R.id.nav_account) {
            Intent intent = new Intent(MainActivity.this, RegisterActivity.class);
            intent.putExtra(USER_ID_BUNDLE_KEY, mAuth.getUid());
            startActivity(intent);
        } else if (id == R.id.nav_manage_users) {
            Intent intent = new Intent(MainActivity.this, ManageUserActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_manage_products) {
            Intent intent = new Intent(MainActivity.this, ManageProductActivity.class);
            startActivity(intent);
        } else if (id == R.id.nav_about) {
            view_About();
        } else if (id == R.id.nav_signout) {
            showLogoutDialog(this);
        } else if (id == R.id.nav_chats) {
            Intent intent = new Intent(MainActivity.this, ManageChats.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    private void showLogoutDialog(final Context context) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage("Are you sure?");
        //builder.setTitle("Delete".toUpperCase());
        builder.setPositiveButton("Yes"
                , new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        signOut();
                    }
                });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                dialog.dismiss();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }


    private void signOut() {
        mAuth.signOut();
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    private void view_About() {
        AlertDialog.Builder alt_bld = new AlertDialog.Builder(MainActivity.this);
        LayoutInflater factory = LayoutInflater.from(MainActivity.this);
        final View textEntryview = factory.inflate(R.layout.about, null);
        alt_bld.setView(textEntryview).setCancelable(true)
                .setNegativeButton(getResources().getString(R.string.about_negative_button)
                        , new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = alt_bld.create();
        alert.setTitle(getResources().getString(R.string.about_title) + " - " + getAppVersion(this));
        alert.setIcon(R.mipmap.ic_launcher);
        alert.show();
    }

    private void startService() {
        Log.d(TAG, "service started");
        Intent intent = new Intent(this, APIService.class);
        // add infos for the service which file to download and where to store
        intent.putExtra(AppConstants.USER_ID_BUNDLE_KEY, mAuth.getUid());
        //intent.putExtra(DownloadService.URL, "http://www.vogella.com/index.html");
        startService(intent);
    }

}
