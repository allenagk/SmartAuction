package com.android.smartauction.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.android.smartauction.R;
import com.android.smartauction.Util;
import com.android.smartauction.adapter.MessageViewHolder;
import com.android.smartauction.adapter.PostViewHolder;
import com.android.smartauction.model.ChatMessage;
import com.android.smartauction.model.Post;
import com.android.smartauction.model.User;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.android.smartauction.AppConstants.MESSAGES_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.USERS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.USER_ID_BUNDLE_KEY;
import static com.android.smartauction.Util.getLocalPrice;

public class ChatActivity extends Activity {

    private static final String TAG = "ChatActivity";
    private FirebaseFirestore mDb;
    private FirebaseAuth mAuth;

    private String endUser;
    private String endUserName;

    private FirestoreRecyclerAdapter mAdapter1;
    private RecyclerView mSenderMessageList;

    private FirestoreRecyclerAdapter mAdapter2;
    private RecyclerView mReceiverMessageList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mDb = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        if (getIntent().hasExtra(USER_ID_BUNDLE_KEY)) {
                endUser = getIntent().getStringExtra(USER_ID_BUNDLE_KEY);

            Log.d(TAG, "END USER: " + endUser);
        } else {
            Toast.makeText(ChatActivity.this, "Something went wrong, Please try later",
                    Toast.LENGTH_LONG).show();
            finish();
        }

        FloatingActionButton fab =
                (FloatingActionButton)findViewById(R.id.fab);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText input = findViewById(R.id.input);
                // Read the input field and push a new instance
                // of ChatMessage to the Firebase database

                publishMessage(mAuth.getUid(), endUser, new ChatMessage(input.getText().toString(), mAuth.getUid()));
                publishMessage(endUser, mAuth.getUid(), new ChatMessage(input.getText().toString(), endUserName));

                // Clear the input
                input.setText("");
            }
        });

        setupSenderEndChat();
        //setupReceiverEndChat();
    }

    private void setupSenderEndChat() {
        mSenderMessageList = findViewById(R.id.start_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mSenderMessageList.addItemDecoration(new DividerItemDecoration(mSenderMessageList.getContext(),
                LinearLayoutManager.VERTICAL));
        mSenderMessageList.setLayoutManager(linearLayoutManager);
        mSenderMessageList.setItemAnimator(new DefaultItemAnimator());

    }

    private void setupReceiverEndChat() {
        /*mReceiverMessageList = findViewById(R.id.end_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mReceiverMessageList.addItemDecoration(new DividerItemDecoration(mReceiverMessageList.getContext(),
                LinearLayoutManager.VERTICAL));
        mReceiverMessageList.setLayoutManager(linearLayoutManager);
        mReceiverMessageList.setItemAnimator(new DefaultItemAnimator());*/
    }

    @Override
    protected void onResume() {
        super.onResume();

        getSenderChatList();
        //getReceiverChatList();

    }

    private void getSenderChatList() {
        Query query = mDb.collection(MESSAGES_COLLECTION_REF)
                .document(mAuth.getUid() + "_" +endUser)
                .collection("chats")
                .orderBy("messageTime");

        FirestoreRecyclerOptions<ChatMessage> response = new FirestoreRecyclerOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .build();

        mAdapter1 = new FirestoreRecyclerAdapter<ChatMessage, MessageViewHolder>(response) {

            @Override
            public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.message, parent, false);

                return new MessageViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

            @Override
            protected void onBindViewHolder(@NonNull MessageViewHolder holder, int position, @NonNull ChatMessage model) {
                holder.setM_user(model.getMessageUser());
                holder.setM_time(Util.convertTime(model.getMessageTime()));
                holder.setM_text(model.getMessageText());
            }

        };

        mAdapter1.startListening();
        mAdapter1.notifyDataSetChanged();
        mSenderMessageList.setAdapter(mAdapter1);
    }

    private void getReceiverChatList() {
        /*Query query = mDb.collection(MESSAGES_COLLECTION_REF)
                .document(endUser + "_" +mAuth.getUid())
                .collection("chats");

        FirestoreRecyclerOptions<ChatMessage> response = new FirestoreRecyclerOptions.Builder<ChatMessage>()
                .setQuery(query, ChatMessage.class)
                .build();

        mAdapter2 = new FirestoreRecyclerAdapter<ChatMessage, MessageViewHolder>(response) {

            @Override
            public MessageViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.message, parent, false);

                return new MessageViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

            @Override
            protected void onBindViewHolder(@NonNull MessageViewHolder holder, int position, @NonNull ChatMessage model) {
                holder.setM_user(model.getMessageUser());
                holder.setM_time(Util.convertTime(model.getMessageTime()));
                holder.setM_text(model.getMessageText());
            }

        };

        mAdapter2.startListening();
        mAdapter2.notifyDataSetChanged();
        mReceiverMessageList.setAdapter(mAdapter2);*/
    }

    private void publishMessage(String startUser, String endUser, ChatMessage chatMessage) {
        mDb.collection(MESSAGES_COLLECTION_REF)
                .document(startUser + "_" + endUser)
                .collection("chats")
                .document()
                .set(chatMessage)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                Toast.makeText(ChatActivity.this, "Sent",
                        Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                Toast.makeText(ChatActivity.this, "Something went wrong, Please try later",
                        Toast.LENGTH_LONG).show();
            }
        });

        Map<String, Object> docData = new HashMap<>();
        docData.put("end_user", endUser);
        mDb.collection(MESSAGES_COLLECTION_REF).document(startUser + "_" + endUser).set(docData);
    }
}
