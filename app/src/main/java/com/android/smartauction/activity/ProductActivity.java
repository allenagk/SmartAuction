package com.android.smartauction.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.smartauction.R;
import com.android.smartauction.Util;
import com.android.smartauction.map.MapsActivity;
import com.android.smartauction.model.Post;
import com.android.smartauction.model.Rating;
import com.android.smartauction.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.squareup.picasso.Picasso;

import java.util.HashMap;

import static com.android.smartauction.AppConstants.ADMIN_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.FROM_AUCTIONS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.FROM_MY_BIDS_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.PARCELABLE_LAT_REF;
import static com.android.smartauction.AppConstants.PARCELABLE_LONG_REF;
import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.smartauction.AppConstants.USERS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.USER_ID_BUNDLE_KEY;

public class ProductActivity extends AppCompatActivity {

    private String postDocID;

    private static final String TAG = "ProductActivity";
    private FirebaseFirestore db;

    private TextView tvTitle;
    //private TextView tvDeliveryState;
    private TextView tvPrice;
    private TextView tvDesc;
    private TextView tvCurrentBid;
    private TextView tvProductAddress;
    private TextView tvAuctioner;
    private RatingBar ratingBar;
    private ImageView ivPoster;
    private EditText etBidAmount;
    //private EditText etAddress;

    private ScrollView scrollView;
    private ProgressBar progressBar;
    private FirebaseAuth mAuth;
    private ProgressDialog mProgress;

    Post post = null;
    private boolean isAdmin;
    private boolean from_mybids;
    private boolean from_auctions;

    private String postOwner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();

        //Initialize the ui by findViewById
        initView();

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            if (getIntent().hasExtra(FROM_MY_BIDS_BUNDLE_KEY)) {
                from_mybids = getIntent().getBooleanExtra(FROM_MY_BIDS_BUNDLE_KEY, false);
            }
            if (getIntent().hasExtra(FROM_AUCTIONS_BUNDLE_KEY)) {
                from_auctions = getIntent().getBooleanExtra(FROM_AUCTIONS_BUNDLE_KEY, false);
            }
            if (getIntent().hasExtra(POST_DOC_ID_BUNDLE_KEY)) {
                postDocID = extras.getString(POST_DOC_ID_BUNDLE_KEY);
                Log.d("POST DOC ID: ", postDocID);
                getPost(postDocID);
                isAdmin = extras.getBoolean(ADMIN_BUNDLE_KEY, false);
                if (isAdmin) {
                    setupAdminUI();
                }
            }
        } else {
            finish();
        }
    }

    //Initialize the ui by findviewbyid
    private void initView() {

        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.please_wait));
        scrollView = findViewById(R.id.scrollView);
        progressBar = findViewById(R.id.progressBar);
        ivPoster = findViewById(R.id.image_view_big);
        tvCurrentBid = findViewById(R.id.tv_current_bid);
        tvProductAddress = findViewById(R.id.tv_pd_location);
        tvAuctioner = findViewById(R.id.tv_pd_auctioner);
        ratingBar = findViewById(R.id.ratingBar);
        //etAddress = findViewById(R.id.et_address);
        //etAddress.setText(address);

        tvTitle = findViewById(R.id.tv_pd_title);
        tvPrice = findViewById(R.id.tv_pd_price);
        //tvDeliveryState = findViewById(R.id.tv_pd_delivery_state);
        tvDesc = findViewById(R.id.tv_pd_desc);

        etBidAmount = findViewById(R.id.et_bid_amount);
    }

    //Set page status load or loaded
    private void setLoading(boolean loading) {
        if (loading) {
            scrollView.setVisibility(View.GONE);
            progressBar.setVisibility(View.VISIBLE);
        } else {
            scrollView.setVisibility(View.VISIBLE);
            progressBar.setVisibility(View.GONE);
        }
    }

    //Send request to get post details, if success set the textViews, else show error
    private void getPost(String doc_id) {
        setLoading(true);
        DocumentReference docRef = db.collection(POSTS_COLLECTION_REF).document(doc_id);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    setLoading(false);
                    post = documentSnapshot.toObject(Post.class);
                    postOwner = post.getUser_id();
                    Log.d(TAG, "Post Title" + " => " + post.getTitle());
                    try {
                        //if (Calendar.getInstance().getTime().before(post.getEnd_date())) {
                        setViews(post);
                        //If post owner disable bid btn and display End Bid button
                        if (isPostOwner(post)) {
                            setupOwnerUI();
                        } else {
                            if (from_auctions) {
                                findViewById(R.id.btn_end_bid).setVisibility(View.GONE);
                                findViewById(R.id.btn_order).setVisibility(View.GONE);
                                findViewById(R.id.btn_view_detail).setVisibility(View.VISIBLE);
                                //findViewById(R.id.btn_complete_transaction).setVisibility(View.VISIBLE);
                            }

                            if (from_mybids) {
                                findViewById(R.id.btn_end_bid).setVisibility(View.GONE);
                                findViewById(R.id.btn_order).setVisibility(View.GONE);
                                findViewById(R.id.btn_view_detail).setVisibility(View.VISIBLE);
                                findViewById(R.id.btn_complete_transaction).setVisibility(View.VISIBLE);
                            }
                        }
                        //} else {
                        //Toast.makeText(ProductActivity.this,
                        //      getString(R.string.toast_bid_expired), Toast.LENGTH_SHORT).show();
                        //  finish();
                        //}
                    } catch (Exception e) {
                        Toast.makeText(ProductActivity.this,
                                getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(ProductActivity.this,
                            getString(R.string.toast_went_wrong), Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }

    //Sets the owners screen with disabled bid btn and enabled End Bid btn
    private void setupOwnerUI() {
        findViewById(R.id.btn_order).setVisibility(View.GONE);
        findViewById(R.id.btn_end_bid).setVisibility(View.VISIBLE);
        etBidAmount.setEnabled(false);

        if (from_auctions) {
            findViewById(R.id.btn_end_bid).setVisibility(View.GONE);
            findViewById(R.id.btn_order).setVisibility(View.GONE);
            findViewById(R.id.btn_view_detail).setVisibility(View.VISIBLE);
            //findViewById(R.id.btn_complete_transaction).setVisibility(View.VISIBLE);
        }

        if (from_mybids) {
            findViewById(R.id.btn_end_bid).setVisibility(View.GONE);
            findViewById(R.id.btn_order).setVisibility(View.GONE);
            findViewById(R.id.btn_view_detail).setVisibility(View.VISIBLE);
            findViewById(R.id.btn_complete_transaction).setVisibility(View.VISIBLE);
        }
    }

    private void setupAdminUI() {
        findViewById(R.id.btn_order).setVisibility(View.GONE);
        findViewById(R.id.btn_delete_product).setVisibility(View.VISIBLE);
    }

    private void setViews(Post post) throws Exception {

        //Set image poster
        Picasso.get()
                .load(post.getPhoto_url1())//.resize(50, 50)
                //.centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .error(R.drawable.ic_menu_info)
                .into(ivPoster);

        tvTitle.setText(post.getTitle());
        tvPrice.setText(Util.getLocalPrice(post.getPrice()));
        tvDesc.setText(post.getDesc());
        tvCurrentBid.setText(post.getCurrent_bid());
        tvProductAddress.setText(post.getProduct_address());
        etBidAmount.setText(post.getCurrent_bid());
        setTvAuctioner(post.getUser_id());
    }

    private void setTvAuctioner(String userid) {
        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(userid);
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                if (documentSnapshot.exists()) {
                    User mUser = documentSnapshot.toObject(User.class);
                    if (mUser != null) {
                        tvAuctioner.setText(mUser.getFirstName() + " " + mUser.getLastName());
                        ratingBar.setRating(mUser.getRatings());
                    }
                } else {
                    finish();
                }

            }
        });
    }

    public void onPlaceBid(View view) {

        if (!etBidAmount.getText().toString().isEmpty()) {
            if (Double.parseDouble(post.getCurrent_bid())
                    < Double.parseDouble(etBidAmount.getText().toString())) {

                post.setCurrent_bid(etBidAmount.getText().toString());
                post.setMax_bid_uid(mAuth.getUid());
                int nBids = post.getBids() + 1;
                post.setBids(nBids);

                updatePost(post);
            } else {
                Toast.makeText(ProductActivity.this,
                        "Enter bid amount higher than the current bid", Toast.LENGTH_SHORT).show();
            }

        } else {
            Log.d(TAG, String.valueOf(R.string.toast_bid_details_empty));
            Toast.makeText(ProductActivity.this,
                    getString(R.string.toast_bid_details_empty), Toast.LENGTH_SHORT).show();
        }
    }

    private void updatePost(Post post) {
        //update bids++, bidscurrentuser, currentamount
        mProgress.setMessage("Please wait...");
        mProgress.show();
        db.collection(POSTS_COLLECTION_REF).document(postDocID).set(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this, "Bid is yours now",
                        Toast.LENGTH_LONG).show();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    //Checks post is owned by the logged in user
    public boolean isPostOwner(Post post) {
        if (post != null) {
            if (mAuth.getCurrentUser().getUid().equalsIgnoreCase(post.getUser_id())) {
                return true;
            }
        }
        return false;
    }

    //Load product location on map
    public void onLocation(View view) {
        if (post.getGeo_point() != null) {
            Intent intent = new Intent(this, MapsActivity.class);
            intent.putExtra(PARCELABLE_LAT_REF, post.getGeo_point().getLatitude());
            intent.putExtra(PARCELABLE_LONG_REF, post.getGeo_point().getLongitude());
            startActivity(intent);
        } else {
            Toast.makeText(this, "No google location set for this product", Toast.LENGTH_SHORT).show();
        }
    }

    //End bid if the user is owner
    public void onPlaceEndBid(View view) {
        mProgress.setMessage("Please wait...");
        mProgress.show();
        post.setLive(false);
        db.collection(POSTS_COLLECTION_REF).document(postDocID).set(post).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this, "Your Auction ended",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                        Toast.LENGTH_LONG).show();
            }
        });
    }

    //Delete current product from database
    public void onDeleteProduct(View view) {
        mProgress.show();
        db.collection(POSTS_COLLECTION_REF).document(postDocID)
                .delete()
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        mProgress.hide();
                        Toast.makeText(ProductActivity.this, "Successfully deleted the Auction product",
                                Toast.LENGTH_LONG).show();
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        mProgress.hide();
                        Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                                Toast.LENGTH_LONG).show();
                    }
                });
    }

    //View transaction details
    public void onTransDetails(View view) {
        if (from_auctions) {
            if (post.getMax_bid_uid() != null) {
                fetchUserData(post.getMax_bid_uid(), true);
            } else {
                Toast.makeText(ProductActivity.this,
                        "Product has not been sold", Toast.LENGTH_SHORT).show();
            }
        }
        if (from_mybids) {
            fetchUserData(post.getUser_id(), false);
        }
    }

    private void showDialogMessage(String title, String name, String email, String address, final String phone) {
        String message = "Name: " + name + "\nEmail: " + email + "\nAddress: " + address + "\nPhone: " + phone;
        AlertDialog.Builder builder;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            builder = new AlertDialog.Builder(this, android.R.style.Theme_Material_Dialog_Alert);
        } else {
            builder = new AlertDialog.Builder(this);
        }
        builder.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Call", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
                        startActivity(intent);
                    }
                })
                .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // do nothing
                    }
                })
                .setIcon(android.R.drawable.ic_dialog_alert)
                .show();
    }

    private void fetchUserData(String userId, final boolean from_auctions) {
        mProgress.show();

        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(userId);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                mProgress.hide();
                if (documentSnapshot.exists()) {
                    User muser = documentSnapshot.toObject(User.class);
                    if (muser != null) {
                        Log.d(TAG, muser.getFirstName());
                        if (from_auctions) {
                            showDialogMessage("Winner Details",
                                    muser.getFirstName() + " " + muser.getLastName(),
                                    muser.getEmail(),
                                    muser.getAddress(),
                                    muser.getPhone());
                        } else {
                            showDialogMessage("ProductOwner Details",
                                    muser.getFirstName() + " " + muser.getLastName(),
                                    muser.getEmail(),
                                    muser.getAddress(),
                                    muser.getPhone());
                        }

                    }
                } else {
                    Toast.makeText(ProductActivity.this,
                            R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this,
                        R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void onTransComplete(View view) {
        getCurrentRating(post.getUser_id());
    }

    private void showRateDialog(final float rate, final String userId) {
        final Dialog rankDialog = new Dialog(ProductActivity.this, R.style.FullHeightDialog);
        rankDialog.setContentView(R.layout.rank_dialog);
        rankDialog.setCancelable(true);
        final RatingBar ratingBar = (RatingBar) rankDialog.findViewById(R.id.dialog_ratingbar);
        ratingBar.setRating(rate);

        TextView text = (TextView) rankDialog.findViewById(R.id.rank_dialog_text1);
        text.setText("Rate");

        Button updateButton = (Button) rankDialog.findViewById(R.id.rank_dialog_button);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rankDialog.dismiss();
                Toast.makeText(ProductActivity.this,
                        "" + ratingBar.getRating(), Toast.LENGTH_SHORT).show();
                updateUserRate(userId, Math.round((ratingBar.getRating())));
            }
        });
        //now that the dialog is set up, it's time to show it
        rankDialog.show();
    }

    private void getCurrentRating(final String userId) {
        mProgress.show();

        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(userId);

        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                mProgress.hide();
                if (documentSnapshot.exists()) {
                    User muser = documentSnapshot.toObject(User.class);
                    if (muser != null) {
                        Log.d(TAG, muser.getFirstName());

                        showRateDialog(muser.getRatings(), userId);

                    }
                } else {
                    Toast.makeText(ProductActivity.this,
                            R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
                }

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this,
                        R.string.contact_service_provider, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void updateUserRate(final String userId, final int rate) {
        mProgress.show();

        DocumentReference docRef = db.collection(USERS_COLLECTION_REF).document(userId).collection("ratings").document("rate");
        docRef.get().addOnSuccessListener(new OnSuccessListener<DocumentSnapshot>() {
            @Override
            public void onSuccess(DocumentSnapshot documentSnapshot) {
                mProgress.hide();
                if (documentSnapshot.exists()) {
                    Rating rating = documentSnapshot.toObject(Rating.class);
                    int count1 = rating.getS1(), count2 = rating.getS2(), count3 = rating.getS3(), count4 = rating.getS4(), count5 = rating.getS5();
                    if (rate == 1) {
                        count1++;
                        rating.setS1(count1);
                    } else if (rate == 2) {
                        count2 ++;
                        rating.setS2(count2);
                    } else if (rate == 3) {
                        count3 ++;
                        rating.setS3(count3);
                    } else if (rate == 4) {
                        count4  ++;
                        rating.setS4(count4);
                    } else if (rate == 5) {
                        count5  ++;
                        rating.setS5(count5);
                    }
                    db.collection(USERS_COLLECTION_REF).document(userId).collection("ratings").document("rate").set(rating);

                    int calculated_rate = 0;

                    calculated_rate = ((1*count1) + (2*count2) + (3*count3) + (4*count4) + (5*count5))/(count1 + count2 + count3 + count4 + count5);

                    Log.d(TAG, "RATING: "+calculated_rate);

                    //Update ratings
                    HashMap<String, Object> x = new HashMap<>();
                    x.put("ratings", calculated_rate);
                    db.collection(USERS_COLLECTION_REF).document(userId).update(x).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            mProgress.hide();
                            db.collection(POSTS_COLLECTION_REF).document(postDocID).delete();
                            Toast.makeText(ProductActivity.this, "You have successfully completed the transaction",
                                    Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mProgress.hide();
                            Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                                    Toast.LENGTH_LONG).show();
                        }
                    });


                } else {
                    Rating rating = new Rating();
                    rating.setS1(0);
                    rating.setS2(0);
                    rating.setS3(0);
                    rating.setS4(0);
                    rating.setS5(0);

                    if (rate == 1) {
                        rating.setS1(rate);
                    } else if (rate == 2) {
                        rating.setS2(rate);
                    } else if (rate == 3) {
                        rating.setS3(rate);
                    } else if (rate == 4) {
                        rating.setS4(rate);
                    } else if (rate == 5) {
                        rating.setS5(rate);
                    }

                    db.collection(USERS_COLLECTION_REF).document(userId).collection("ratings").document("rate").set(rating);

                    int calculated_rate = 0;

                    calculated_rate = ((1*rating.getS1()) + (2*rating.getS2()) + (3*rating.getS3()) + (4*rating.getS4()) + (5*rating.getS5()))/(rating.getS1() + rating.getS2() + rating.getS3() + rating.getS4() + rating.getS5());

                    Log.d(TAG, "RATING: "+calculated_rate);

                    //Update ratings
                    HashMap<String, Object> x = new HashMap<>();
                    x.put("ratings", calculated_rate);
                    db.collection(USERS_COLLECTION_REF).document(userId).update(x).addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            mProgress.hide();
                            db.collection(POSTS_COLLECTION_REF).document(postDocID).delete();
                            Toast.makeText(ProductActivity.this, "You have successfully completed the transaction",
                                    Toast.LENGTH_LONG).show();
                            finish();
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            mProgress.hide();
                            Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        });


        /*HashMap<String, Object> x = new HashMap<>();
        x.put("ratings", rate);
        db.collection(USERS_COLLECTION_REF).document(userId).collection("ratings").document("rate").update(x).addOnSuccessListener(new OnSuccessListener<Void>() {
            @Override
            public void onSuccess(Void aVoid) {
                mProgress.hide();
                db.collection(POSTS_COLLECTION_REF).document(postDocID).delete();
                Toast.makeText(ProductActivity.this, "You have successfully completed the transaction",
                        Toast.LENGTH_LONG).show();
                finish();
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.hide();
                Toast.makeText(ProductActivity.this, "Something went wrong, Please try later",
                        Toast.LENGTH_LONG).show();
            }
        });*/

    }

    public void onChat(View view) {
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(USER_ID_BUNDLE_KEY, postOwner);
        startActivity(intent);
    }
}
