package com.android.smartauction.activity;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.smartauction.R;
import com.android.smartauction.Util;
import com.android.smartauction.map.MapsActivity;
import com.android.smartauction.model.Post;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.GeoPoint;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

import static com.android.smartauction.AppConstants.PARCELABLE_ADDRESS_REF;
import static com.android.smartauction.AppConstants.PARCELABLE_LAT_REF;
import static com.android.smartauction.AppConstants.PARCELABLE_LONG_REF;
import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POST_IMAGES_STORAGE_REF;

public class AddProductActivity extends AppCompatActivity {

    private int mYear, mMonth, mDay, mHour, mMinute;
    private Date mEnddate;

    private View parentLayout;
    private EditText etTitle;
    private EditText etPrice;
    private EditText etDesc;
    private EditText etDate;
    private EditText etAddress;
    private ImageButton imgBtnPhotoUrl1;
    private Button submit;
    private ProgressDialog mProgress;
    private Uri mImageUri1 = null;
    private static final int MAP_REQUEST_CODE = 0;
    private static final int GALLERY_REQUEST_1 = 1;

    private static final String TAG = "AddProductActivity";

    private FirebaseAuth mAuth;
    private FirebaseStorage mStorage;
    private FirebaseFirestore mDatabase;
    private Post newPost;

    double latitude = 0, longitude = 0;
    String address = null;
    private boolean isFillAddress = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product);

        mAuth = FirebaseAuth.getInstance();
        mStorage = FirebaseStorage.getInstance();
        mDatabase = FirebaseFirestore.getInstance();

        initViews();
    }

    private void initViews() {
        parentLayout = findViewById(android.R.id.content);
        etTitle = findViewById(R.id.et_title);
        etPrice = findViewById(R.id.et_price);
        etDesc = findViewById(R.id.et_desc);
        etDate = findViewById(R.id.et_date);
        etAddress = findViewById(R.id.et_location);
        imgBtnPhotoUrl1 = findViewById(R.id.imgbtn_photo1);

        submit = findViewById(R.id.btn_submit);
        mProgress = new ProgressDialog(this);
        mProgress.setMessage(getString(R.string.please_wait));


    }

    public void onPickDate(View view) {

        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);


        DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {

                        //etDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year);
                        //mEnddate = new Timestamp((year - 1900), monthOfYear, dayOfMonth, 0, 0, 0, 0);
                        setTimeWithDate(year, monthOfYear, dayOfMonth);
                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();
    }

    public void setTimeWithDate(final int mYear, final int mMonth, final int mDay) {
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        TimePickerDialog mTimePicker;
        mTimePicker = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                etDate.setText(mDay + "-" + (mMonth + 1) + "-" + mYear + "  " + selectedHour + ":" + selectedMinute);
                mEnddate = new Timestamp((mYear - 1900), mMonth, mDay, selectedHour, selectedMinute, 0, 0);
                Log.d(TAG, mDay + "-" + (mMonth + 1) + "-" + mYear + "-" + selectedHour + ":" + selectedMinute);
            }
        }, hour, minute, true);//Yes 24 hour time
        mTimePicker.setTitle("Select Time");
        mTimePicker.show();
    }

    public void onAddImage(View view) {
        Intent galleryIntent1 = new Intent(Intent.ACTION_GET_CONTENT);
        galleryIntent1.setType("image/*");
        startActivityForResult(galleryIntent1, GALLERY_REQUEST_1);
    }

    public void onSubmit(View view) {
        String title = etTitle.getText().toString().trim();
        String price = etPrice.getText().toString().trim();
        String desc = etDesc.getText().toString().trim();
        String end_date = etDate.getText().toString().trim();
        GeoPoint geoPoint = new GeoPoint(latitude, longitude);

        if (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(price) && !TextUtils.isEmpty(end_date) && isFillAddress) {

            if (mImageUri1 != null) {

                newPost = new Post(mAuth.getUid(), title, desc, price, geoPoint);
                newPost.setLive(true);
                newPost.setBids(0);
                newPost.setCurrent_bid(price);
                newPost.setEnd_date(mEnddate);
                newPost.setProduct_address(address);

                uploadPhoto(mImageUri1);

            } else {
                Snackbar.make(parentLayout, "You must upload a photo", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }

        } else {
            Snackbar.make(parentLayout, "Please fill all the required fields", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }
    }

    private void uploadPhoto(Uri image_uri) {
        mProgress.setMessage(getString(R.string.please_wait));
        mProgress.show();

        StorageReference filePath = mStorage.getReference()
                .child(POST_IMAGES_STORAGE_REF).child(Util.randomUUID());

        filePath.putFile(image_uri).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                mProgress.dismiss();
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                newPost.setPhoto_url1(downloadUrl.toString());

                //Adding details to database with photoUrl
                writeDocument(newPost);
                Toast.makeText(AddProductActivity.this, "Product has been posted", Toast.LENGTH_LONG).show();
                finish();

            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                mProgress.dismiss();
                //Toast.makeText(AddPostActivity.this, "Something went wrong",
                //      Toast.LENGTH_SHORT).show();
                Snackbar.make(parentLayout, "Something went wrong, Try again later...", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    private void writeDocument(Post post) {
        mDatabase.collection(POSTS_COLLECTION_REF).add(post);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == GALLERY_REQUEST_1 && resultCode == RESULT_OK) {
            mImageUri1 = data.getData();
            imgBtnPhotoUrl1.setScaleType(ImageView.ScaleType.CENTER_INSIDE);
            imgBtnPhotoUrl1.setImageURI(mImageUri1);
        }

        if (requestCode == MAP_REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                isFillAddress = true;
                address = data.getStringExtra(PARCELABLE_ADDRESS_REF);
                etAddress.setText(address);

                latitude = data.getDoubleExtra(PARCELABLE_LAT_REF, 5);
                longitude = data.getDoubleExtra(PARCELABLE_LONG_REF, 10);

                Log.d(TAG, "Address: " + address + " Lat: " + latitude + " Longitude: " + longitude);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Write your code if there's no result
            }
        }

    }

    public void onLocation(View view) {
        Intent intent = new Intent(this, MapsActivity.class);
        startActivityForResult(intent, MAP_REQUEST_CODE);
    }
}
