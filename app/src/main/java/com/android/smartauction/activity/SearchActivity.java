package com.android.smartauction.activity;

import android.app.ActionBar;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.smartauction.R;
import com.android.smartauction.adapter.PostViewHolder;
import com.android.smartauction.model.Post;
import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;

import static com.android.smartauction.AppConstants.POSTS_COLLECTION_REF;
import static com.android.smartauction.AppConstants.POST_DOC_ID_BUNDLE_KEY;
import static com.android.smartauction.Util.getLocalPrice;

public class SearchActivity extends AppCompatActivity {

    private final static String TAG = "SearchActivity";

    private FirestoreRecyclerAdapter mAdapter;
    private RecyclerView mPostList;
    private FirebaseFirestore db;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        db = FirebaseFirestore.getInstance();

        mPostList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mPostList.addItemDecoration(new DividerItemDecoration(mPostList.getContext(),
                LinearLayoutManager.VERTICAL));
        mPostList.setLayoutManager(linearLayoutManager);
        mPostList.setItemAnimator(new DefaultItemAnimator());

        SearchView searchView = new SearchView(this);
        getSupportActionBar().setCustomView(searchView);
        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        searchView.setOnCloseListener(new SearchView.OnCloseListener() {
            @Override
            public boolean onClose() {
                finish();
                return false;
            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {

            @Override
            public boolean onQueryTextSubmit(String query) {
                if (!query.isEmpty()) {
                    getSearchList(query);
                }
                return true;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (!query.isEmpty()) {
                    getSearchList(query);
                }
                return true;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        //getSearchList();
    }

    //Search product from text
    private void getSearchList(String text) {

        Query query = db.collection(POSTS_COLLECTION_REF)
                .orderBy("title").whereEqualTo("live", true)
                .startAt(text).endAt(text + "\uf8ff");

        FirestoreRecyclerOptions<Post> response = new FirestoreRecyclerOptions.Builder<Post>()
                .setQuery(query, Post.class)
                .build();

        mAdapter = new FirestoreRecyclerAdapter<Post, PostViewHolder>(response) {

            @Override
            protected void onBindViewHolder(PostViewHolder holder, int position, Post model) {
                holder.setTitle(model.getTitle());
                holder.setDesc(model.getDesc());
                holder.setStart_bid(getLocalPrice(model.getPrice()));
                holder.setCurrent_bid(getLocalPrice(model.getCurrent_bid()));
                holder.setStart_date(model.getDate().toString());
                holder.setEnd_date(model.getEnd_date().toString());
                holder.setBids(model.getBids());

                holder.setImageView(model.getPhoto_url1());

                final String docId = getSnapshots().getSnapshot(position).getId();
                Log.d("DOC ID: ", docId);
                Log.d("RESULT TITLE: ", model.getTitle());
                //Log.d("RESULT DATE: ", model.getDate().toString());

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(SearchActivity.this, ProductActivity.class);
                        intent.putExtra(POST_DOC_ID_BUNDLE_KEY, docId);
                        startActivity(intent);
                        Log.d(TAG, docId.toString());
                    }
                });
            }

            @Override
            public PostViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
                View view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.product_list_item, parent, false);

                return new PostViewHolder(view);
            }

            @Override
            public void onError(FirebaseFirestoreException e) {
                Log.e(TAG, "error : " + e.getMessage());
            }

        };

        mAdapter.startListening();
        mAdapter.notifyDataSetChanged();
        mPostList.setAdapter(mAdapter);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}
