package com.android.smartauction.activity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.android.smartauction.R;
import com.android.smartauction.Util;
import com.android.smartauction.adapter.MessageListViewHolder;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.ArrayList;
import java.util.List;

import static com.android.smartauction.AppConstants.MESSAGES_COLLECTION_REF;
import static com.android.smartauction.AppConstants.USERS_COLLECTION_REF;

public class ManageChats extends AppCompatActivity {

    private final static String TAG = "ManageChats";
    private FirebaseFirestore db;

    private FirebaseAuth mAuth;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView mChatList;
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);

        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage("Please wait...");
        mProgressDialog.setCancelable(false);


        mChatList = findViewById(R.id.post_list);
        RecyclerView.LayoutManager linearLayoutManager = new LinearLayoutManager(getApplicationContext());
        mChatList.addItemDecoration(new DividerItemDecoration(mChatList.getContext(),
                LinearLayoutManager.VERTICAL));
        mChatList.setLayoutManager(linearLayoutManager);
        mChatList.setItemAnimator(new DefaultItemAnimator());
    }

    @Override
    protected void onResume() {
        super.onResume();

        getChatList(mAuth.getUid());
    }

    private void getChatList(final String userId) {

        db.collection(MESSAGES_COLLECTION_REF).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {

                    List<String> list = new ArrayList<>();

                    for (DocumentSnapshot document : task.getResult()) {
                        if (Util.getSender(document.getId()).equalsIgnoreCase(userId)) {
                            list.add(document.getId());
                        }
                    }
                    mAdapter = new MessageListViewHolder(list, getApplicationContext());
                    mChatList.setAdapter(mAdapter);
                    Log.d(TAG, list.toString());
                } else {
                    Log.d(TAG, "Error getting documents: ", task.getException());
                }
            }
        });
    }
}
