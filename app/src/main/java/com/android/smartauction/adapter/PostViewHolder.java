package com.android.smartauction.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.smartauction.R;
import com.squareup.picasso.Picasso;

public class PostViewHolder extends RecyclerView.ViewHolder {


    View mView;

    public PostViewHolder(View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void setTitle(String title) {
        TextView post_title = mView.findViewById(R.id.tv_title);
        post_title.setText(title);
    }

    public void setDesc(String desc) {
        TextView post_desc = mView.findViewById(R.id.tv_desc);
        post_desc.setText(desc);
    }

    public void setStart_bid(String start_bid) {
        TextView tv_start_bid = mView.findViewById(R.id.tv_start_bid);
        tv_start_bid.setText(start_bid);
    }

    public void setCurrent_bid(String current_bid) {
        TextView tv_current_bid = mView.findViewById(R.id.tv_current_bid);
        tv_current_bid.setText(current_bid);
    }

    public void setStart_date(String start_date) {
        TextView tv_auc_start = mView.findViewById(R.id.tv_auc_start);
        tv_auc_start.setText(start_date);
    }

    public void setEnd_date(String end_date) {
        TextView tv_auc_end = mView.findViewById(R.id.tv_auc_end);
        tv_auc_end.setText(end_date);
    }

    public void setBids(int bids) {
        TextView tv_bids = mView.findViewById(R.id.tv_bids);
        tv_bids.setText(bids + "bids");
    }

    public void setImageView(String url) {
        ImageView imagePost = mView.findViewById(R.id.img_product);
        Picasso.get()
                .load(url)//.resize(50, 50)
                //.centerCrop()
                //.networkPolicy(NetworkPolicy.OFFLINE)
                .error(R.drawable.ic_menu_info)
                .into(imagePost);
    }

}
