package com.android.smartauction.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.android.smartauction.R;

/**
 * Created by savantis-allen on 12/5/18.
 */
public class MessageViewHolder  extends RecyclerView.ViewHolder {

    View mView;

    public MessageViewHolder(View itemView) {
        super(itemView);

        mView = itemView;
    }

    public void setM_user(String m_user) {
        TextView message_user = mView.findViewById(R.id.message_user);
        message_user.setText(m_user);
    }

    public void setM_time(String m_time) {
        TextView message_time = mView.findViewById(R.id.message_time);
        message_time.setText(m_time);
    }

    public void setM_text(String m_text) {
        TextView message_text = mView.findViewById(R.id.message_text);
        message_text.setText(m_text);
    }
}
