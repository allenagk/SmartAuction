package com.android.smartauction;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.CountDownTimer;
import android.util.Log;

import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

public class Util {
    //Generate random String UUID
    public static String randomUUID() {
        String randomUUID = UUID.randomUUID().toString();
        return randomUUID;
    }

    public static String getImageNameFromUri(Uri uri) {
        String url = uri.toString();
        String fileName = url.substring(url.lastIndexOf('/') + 1);
        return fileName;
    }

    public static String getLocalPrice(String price) {
        String stringPrice = "Rs. " + price;
        return stringPrice;
    }

    public static String getAppVersion(Context context) {
        String version = null;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
            version = pInfo.versionName;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return version;
    }

    public static void startCountDown() {
        new CountDownTimer(30000, 1000) {

            public void onTick(long millisUntilFinished) {
                long seconds = millisUntilFinished / 1000;
                Log.d("TIME", "seconds remaining: " + seconds);
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                Log.d("TIME", "done!");
            }

        }.start();
    }

    public static String convertTime(long time){
        Date date = new Date(time);
        Format format = new SimpleDateFormat("yyyy MM dd HH:mm:ss");
        return format.format(date);
    }

    public static  String getSender(String raw) {
        String actual[] = raw.split("_");
        return actual[0];
    }

    public static  String getReceiver(String raw) {
        String actual[] = raw.split("_");
        return actual[1];
    }
}
